package require cmdline
set parameters {
    {jobs.arg  		1 		"How many threads to use"}
    {build_dir.arg  "." 	"Build directory"}
    {board_name.arg "" 		"Board name. 	eg. vc707"}
    {board_ver.arg  "" 		"Board version.	eg. 1.3"}
    {board_part.arg "" 		"Board part. 	eg. xc7vx485tffg1761-2"}
}
set usage "- Script used to generate the bitstream"
array set options [cmdline::getoptions ::argv $parameters $usage]


setws $options(build_dir)/xvc_microserver.sdk

createhw 	-name xvc_microserver_hw  -hwspec $options(build_dir)/xvc_microserver.sdk/base_wrapper.hdf


switch -glob $options(board_name) \
{
    zc*     { set processor ps7_cortexa9_0 }
    default { set processor microblaze_0   }
}


# Other alternative freertos823_xilinx
createbsp 	-name xvc_microserver_bsp -hwproject xvc_microserver_hw -proc $processor -os standalone

createapp	-name xvc_microserver_app -hwproject xvc_microserver_hw -proc $processor -os standalone -lang C -app {Empty Application} -bsp xvc_microserver_bsp

projects -clean
projects -build


exit



#configbsp 	-bsp xvc_microserver_bsp config_time true
#updatemss 	-mss xvc_microserver_bsp/system.mss
#regenbsp 	-bsp xvc_microserver_bsp

#closehw $BUILD_DIR/xvc_microserver.sdk/base_mb_wrapper.hdf
