package require cmdline
set parameters {
    {jobs.arg  		1 		"How many threads to use"}
    {build_dir.arg  "." 	"Build directory"}
    {board_name.arg "" 		"Board name. 	eg. zc706"}
    {board_ver.arg  "" 		"Board version.	eg. 1.3"}
    {board_part.arg "" 		"Board part. 	eg. xc7z045ffg900-2"}
}
set usage "- Script used to generate the bitstream"
array set options [cmdline::getoptions ::argv $parameters $usage]

# Creating new project

create_project xvc_microserver $options(build_dir) -part $options(board_part)
set_property board_part xilinx.com:$options(board_name):part0:$options(board_ver) [current_project]
set_property target_language Verilog [current_project]

create_bd_design "base_zynq" -mode batch

create_bd_cell -type ip -vlnv xilinx.com:ip:processing_system7:5.5		processing_system7_0
create_bd_cell -type ip -vlnv xilinx.com:ip:axi_gpio:2.0				axi_gpio_0
create_bd_cell -type ip -vlnv xilinx.com:ip:blk_mem_gen:8.3				blk_mem_gen_0
create_bd_cell -type ip -vlnv xilinx.com:ip:axi_bram_ctrl:4.0			axi_bram_ctrl_0
create_bd_cell -type ip -vlnv xilinx.com:ip:proc_sys_reset:5.0			rst_ps7_0_50M
#create_bd_cell -type ip -vlnv xilinx.com:ip:axi_crossbar:2.1			xbar
#create_bd_cell -type ip -vlnv xilinx.com:ip:axi_protocol_converter:2.1	auto_pc
#create_bd_cell -type ip -vlnv xilinx.com:ip:axi_protocol_converter:2.1	auto_pc

# Performing connections
startgroup
apply_bd_automation -rule xilinx.com:bd_rule:processing_system7 -config {make_external "FIXED_IO, DDR" apply_board_preset "1" Master "Disable" Slave "Disable" }  [get_bd_cells processing_system7_0]
apply_bd_automation -rule xilinx.com:bd_rule:axi4 		-config {Master "/processing_system7_0/M_AXI_GP0" Clk "Auto" }  	[get_bd_intf_pins axi_gpio_0/S_AXI]
apply_bd_automation -rule xilinx.com:bd_rule:board		-config {Board_Interface "dip_switches_4bits ( Dip switches ) " }	[get_bd_intf_pins axi_gpio_0/GPIO]
apply_bd_automation -rule xilinx.com:bd_rule:axi4 		-config {Master "/processing_system7_0/M_AXI_GP0" Clk "Auto" }  	[get_bd_intf_pins axi_bram_ctrl_0/S_AXI]
apply_bd_automation -rule xilinx.com:bd_rule:bram_cntlr	-config {BRAM "/blk_mem_gen_0" }									[get_bd_intf_pins axi_bram_ctrl_0/BRAM_PORTA]
apply_bd_automation -rule xilinx.com:bd_rule:bram_cntlr	-config {BRAM "/blk_mem_gen_0" }									[get_bd_intf_pins axi_bram_ctrl_0/BRAM_PORTB]
#apply_bd_automation -rule xilinx.com:bd_rule:axi4 		-config {Slave "/axi_bram_ctrl_0/S_AXI" Clk "Auto" }				[get_bd_intf_pins xbar/M00_AXI]
#apply_bd_automation -rule xilinx.com:bd_rule:axi4 		-config {Slave "/axi_bram_ctrl_0/S_AXI" Clk "Auto" }				[get_bd_intf_pins xbar/M01_AXI]
endgroup


# Setting preset for IO connections (it already integrate the ethernet controller and the timer)
set_property -dict [list CONFIG.preset $options(board_name) ] [get_bd_cells processing_system7_0]

# Regenerate Layout and validationg
regenerate_bd_layout
validate_bd_design

# Saving and closing
save_bd_design
close_bd_design [get_bd_designs base_zynq]


# Generationg wrapper
make_wrapper -files [get_files $options(build_dir)/xvc_microserver.srcs/sources_1/bd/base_zynq/base_zynq.bd] -top
add_files -norecurse $options(build_dir)/xvc_microserver.srcs/sources_1/bd/base_zynq/hdl/base_zynq_wrapper.v
update_compile_order -fileset sources_1
update_compile_order -fileset sim_1



# Closing prject
close_project


