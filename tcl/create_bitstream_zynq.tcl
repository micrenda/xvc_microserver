package require cmdline
set parameters {
    {jobs.arg  		1 		"How many threads to use"}
    {build_dir.arg  "." 	"Build directory"}
    {board_name.arg "" 		"Board name. 	eg. zc706"}
    {board_ver.arg  "" 		"Board version.	eg. 1.3"}
    {board_part.arg "" 		"Board part. 	eg. xc7z045ffg900-2"}
}
set usage "- Script used to generate the bitstream"
array set options [cmdline::getoptions ::argv $parameters $usage]


# Opening project
open_project $options(build_dir)/xvc_microserver.xpr

# Resetting project
reset_target all 					[get_files  $options(build_dir)/xvc_microserver.srcs/sources_1/bd/base_zynq/base_zynq.bd]
export_ip_user_files -of_objects	[get_files  $options(build_dir)/xvc_microserver.srcs/sources_1/bd/base_zynq/base_zynq.bd] -sync -no_script -force -quiet
delete_ip_run 						[get_files -of_objects [get_fileset sources_1] $options(build_dir)/xvc_microserver.srcs/sources_1/bd/base_zynq/base_zynq.bd]


# Generate target files
generate_target all                     [get_files $options(build_dir)/xvc_microserver.srcs/sources_1/bd/base_zynq/base_zynq.bd]
export_ip_user_files -of_objects        [get_files $options(build_dir)/xvc_microserver.srcs/sources_1/bd/base_zynq/base_zynq.bd] -no_script -sync -force -quiet
create_ip_run [get_files -of_objects    [get_fileset sources_1] $options(build_dir)/xvc_microserver.srcs/sources_1/bd/base_zynq/base_zynq.bd]
launch_runs synth_1 -jobs $options(jobs)       	 
wait_on_run synth_1					
                    
            
export_simulation                                                                                               		\
                -of_objects [get_files $options(build_dir)/xvc_microserver.srcs/sources_1/bd/base_zynq/base_zynq.bd]    \
                -directory $options(build_dir)/xvc_microserver.ip_user_files/sim_scripts                             	\
                -ip_user_files_dir $options(build_dir)/xvc_microserver.ip_user_files                                 	\
                -ipstatic_source_dir $options(build_dir)/xvc_microserver.ip_user_files/ipstatic                      	\
                -lib_map_path [list                                                                             		\
                    {modelsim=$options(build_dir)/xvc_microserver.cache/compile_simlib/modelsim}                     	\
                    {questa=$options(build_dir)/xvc_microserver.cache/compile_simlib/questa}                         	\
                    {ies=$options(build_dir)/xvc_microserver.cache/compile_simlib/ies}                               	\
                    {vcs=$options(build_dir)/xvc_microserver.cache/compile_simlib/vcs}                               	\
                    {riviera=$options(build_dir)/xvc_microserver.cache/compile_simlib/riviera}                       	\
                ]                                                                                               		\
                -use_ip_compiled_libs                                                                           		\
                -force                                                                                          		\
                -quiet

# Creating bitstream (for now stopping to route_design, due to ip-core license issue. Move to write_bitstream when resolved)
launch_runs impl_1 -to_step write_bitstream -jobs $options(jobs)
wait_on_run impl_1


# Closing project
close_project
