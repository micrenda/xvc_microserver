package require cmdline
set parameters {
    {jobs.arg  		1 		"How many threads to use"}
    {build_dir.arg  "." 	"Build directory"}
    {board_name.arg "" 		"Board name. 	eg. vc707"}
    {board_ver.arg  "" 		"Board version.	eg. 1.3"}
    {board_part.arg "" 		"Board part. 	eg. xc7vx485tffg1761-2"}
}
set usage "- Script used to generate the bitstream"
array set options [cmdline::getoptions ::argv $parameters $usage]

setws $options(build_dir)/xvc_microserver.sdk

configapp -app xvc_microserver_app -add include-path \$\{workspace_loc:/xvc_microserver_app/src\}
configapp -app xvc_microserver_app -add include-path \$\{workspace_loc:/xvc_microserver_app/uip\}

projects -clean
projects -build
