package require cmdline
set parameters {
    {jobs.arg  		1 		"How many threads to use"}
    {build_dir.arg  "." 	"Build directory"}
    {board_name.arg "" 		"Board name. 	eg. vc707"}
    {board_ver.arg  "" 		"Board version.	eg. 1.3"}
    {board_part.arg "" 		"Board part. 	eg. xc7vx485tffg1761-2"}
}
set usage "- Script used to generate the bitstream"
array set options [cmdline::getoptions ::argv $parameters $usage]

# Creating new project

create_project xvc_microserver $options(build_dir) -part $options(board_part)
set_property board_part xilinx.com:$options(board_name):part0:$options(board_ver) [current_project]
set_property target_language Verilog [current_project]

create_bd_design "base_mb" -mode batch



# Adding and personalizing microblaze_0 processor
startgroup
create_bd_cell -type ip -vlnv xilinx.com:ip:microblaze:10.0 microblaze_0
set_property -dict [                                    \
	list                                                \
		CONFIG.C_D_AXI {1}                              \
		CONFIG.C_USE_MSR_INSTR {1}                      \
		CONFIG.C_USE_PCMP_INSTR {1}                     \
		CONFIG.C_USE_BARREL {1}                         \
		CONFIG.C_USE_DIV {1}                            \
		CONFIG.C_USE_HW_MUL {1}                         \
		CONFIG.G_USE_EXCEPTIONS {1}                     \
														\
		CONFIG.C_UNALIGNED_EXCEPTIONS {0}				\
		CONFIG.C_ILL_OPCODE_EXCEPTION {0}				\
		CONFIG.C_M_AXI_D_BUS_EXCEPTION {1}				\
		CONFIG.C_DIV_ZERO_EXCEPTION {1}					\
		CONFIG.C_USE_STACK_PROTECTION {0}				\
		CONFIG.C_OPCODE_0x0_ILLEGAL {0}					\
	] [get_bd_cells microblaze_0]                       
endgroup

# Adding the other components
startgroup
apply_bd_automation -rule xilinx.com:bd_rule:microblaze -config {       \
	local_mem "128KB"                                                   \
	ecc "None"                                                          \
	cache "None"                                                        \
	debug_module "Debug Only"                                           \
	axi_periph "Enabled"                                                \
	axi_intc "1"                                                        \
	clk "New Clocking Wizard (100 MHz)"                                 \
 }  [get_bd_cells microblaze_0]
endgroup

# Performing connections for all the other components
startgroup
apply_bd_automation -rule xilinx.com:bd_rule:board -config {Board_Interface "sys_diff_clock ( System differential clock ) " }  [get_bd_intf_pins clk_wiz_1/CLK_IN1_D]
apply_bd_automation -rule xilinx.com:bd_rule:board -config {Board_Interface "reset ( FPGA Reset ) " }  [get_bd_pins clk_wiz_1/reset]
apply_bd_automation -rule xilinx.com:bd_rule:board -config {Board_Interface "reset ( FPGA Reset ) " }  [get_bd_pins rst_clk_wiz_1_100M/ext_reset_in]
endgroup

# Adding an axi ethernet controller and creating connections
startgroup
create_bd_cell -type ip -vlnv xilinx.com:ip:axi_ethernet:7.0 axi_ethernet_0

apply_bd_automation -rule xilinx.com:bd_rule:axi_ethernet   -config {PHY_TYPE "SGMII" FIFO_DMA "DMA" }             			[get_bd_cells axi_ethernet_0]

# Enable SG mode for ethernet DMA
set_property -dict [list CONFIG.c_include_sg {1} CONFIG.c_sg_include_stscntrl_strm {1}] [get_bd_cells axi_ethernet_0_dma]

apply_bd_automation -rule xilinx.com:bd_rule:axi4  			-config {Master "/microblaze_0 (Periph)" Clk "Auto" }  			[get_bd_intf_pins axi_ethernet_0/s_axi]
apply_bd_automation -rule xilinx.com:bd_rule:board 			-config {Board_Interface "mdio_mdc ( Onboard PHY ) " }  		[get_bd_intf_pins axi_ethernet_0/mdio]
apply_bd_automation -rule xilinx.com:bd_rule:board 			-config {Board_Interface "sgmii ( Onboard PHY ) " }  			[get_bd_intf_pins axi_ethernet_0/sgmii]
apply_bd_automation -rule xilinx.com:bd_rule:board 			-config {Board_Interface "sgmii_mgt_clk ( SGMII MGT clock ) " }	[get_bd_intf_pins axi_ethernet_0/mgt_clk]
apply_bd_automation -rule xilinx.com:bd_rule:board 			-config {Board_Interface "phy_reset_out ( Onboard PHY ) " }  	[get_bd_pins axi_ethernet_0/phy_rst_n]
apply_bd_automation -rule xilinx.com:bd_rule:axi4  			-config {Master "/microblaze_0 (Periph)" Clk "Auto" }  			[get_bd_intf_pins axi_ethernet_0_dma/S_AXI_LITE]

# Fixes a mistake of automation 
delete_bd_objs [get_bd_nets -of_objects [get_bd_pins axi_ethernet_0/axis_clk]]
connect_bd_net [get_bd_pins axi_ethernet_0/axis_clk] [get_bd_pins clk_wiz_1/clk_out1]
connect_bd_net [get_bd_pins axi_ethernet_0_dma/m_axi_mm2s_aclk] [get_bd_pins clk_wiz_1/clk_out1]
connect_bd_net [get_bd_pins axi_ethernet_0_dma/m_axi_s2mm_aclk] [get_bd_pins clk_wiz_1/clk_out1]
connect_bd_net [get_bd_pins axi_ethernet_0_dma/m_axi_sg_aclk] [get_bd_pins clk_wiz_1/clk_out1] 

# Setting the signal detect value tied to 1
create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant:1.1 xlconstant_eth_signal_detect_0
set_property -dict [list CONFIG.CONST_VAL {1}] [get_bd_cells xlconstant_eth_signal_detect_0]
connect_bd_net [get_bd_pins xlconstant_eth_signal_detect_0/dout] [get_bd_pins axi_ethernet_0/signal_detect]

endgroup


# Adding an axi timer and creating connections
startgroup
create_bd_cell -type ip -vlnv xilinx.com:ip:axi_timer:2.0 axi_timer_0
set_property -dict [list CONFIG.mode_64bit {1}] [get_bd_cells axi_timer_0]
apply_bd_automation -rule xilinx.com:bd_rule:axi4 			-config {Master "/microblaze_0 (Periph)" Clk "Auto" }  	[get_bd_intf_pins axi_timer_0/S_AXI]
endgroup

# Adding an axi uart and creating connections
startgroup
create_bd_cell -type ip -vlnv xilinx.com:ip:axi_uartlite:2.0 axi_uartlite_0
apply_bd_automation -rule xilinx.com:bd_rule:axi4	-config {Master "/microblaze_0 (Periph)" Clk "Auto" }  	[get_bd_intf_pins axi_uartlite_0/S_AXI]
apply_bd_automation -rule xilinx.com:bd_rule:board	-config {Board_Interface "rs232_uart ( UART ) " }  		[get_bd_intf_pins axi_uartlite_0/UART]
endgroup


# Connecting interupts
startgroup
set_property -dict [list CONFIG.NUM_PORTS {3}] [get_bd_cells microblaze_0_xlconcat]
connect_bd_net [get_bd_pins axi_ethernet_0/interrupt]           [get_bd_pins microblaze_0_xlconcat/In0]
connect_bd_net [get_bd_pins axi_timer_0/interrupt] 				[get_bd_pins microblaze_0_xlconcat/In1]
connect_bd_net [get_bd_pins axi_uartlite_0/interrupt] 			[get_bd_pins microblaze_0_xlconcat/In2]
endgroup

# Regenerate Layout
regenerate_bd_layout
save_bd_design

# Validating Design
validate_bd_design
save_bd_design

# Close Design
close_bd_design [get_bd_designs base_mb]

# Generationg wrapper
make_wrapper -files [get_files $options(build_dir)/xvc_microserver.srcs/sources_1/bd/base_mb/base_mb.bd] -top
add_files -norecurse $options(build_dir)/xvc_microserver.srcs/sources_1/bd/base_mb/hdl/base_mb_wrapper.v
update_compile_order -fileset sources_1
update_compile_order -fileset sim_1

# Closing prject
close_project
