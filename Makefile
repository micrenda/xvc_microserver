BASE        = $(shell pwd)
TCL         = $(BASE)/tcl
BUILD_DIR   = $(BASE)/build
JOBS        = 8
			  
ifeq ($(BOARD),vc707)
	BOARD_TYPE=microblaze
	BOARD_DIR=VC707
	BOARD_NAME=vc707
	BOARD_VER=1.3
	BOARD_PART=xc7vx485tffg1761-2
else ifeq ($(BOARD),zc706)
	BOARD_TYPE=zynq
	BOARD_DIR=ZC706
	BOARD_NAME=zc706
	BOARD_VER=1.3
	BOARD_PART=xc7z045ffg900-2
endif

PHONY: check clean 

	
check:
	@if [ -z "$(XILINX_VIVADO)" ]; then                 \
		echo "XILINX_VIVADO env variable was not set";  \
		exit 1; \
	fi

	@if [ -z "$(XILINX_SDK)" ]; then                    \
		echo "XILINX_SDK env variable was not set";     \
		exit 1; \
	fi

	@if [ -z "$(BOARD)" ]; then \
		echo "To compile this project you have to pass the parameter BOARD=<board_name> where <board_name> is one of: vc707, zc706";    \
		exit 1; \
	fi
	

clean-all:
	rm -rf $(BUILD_DIR)

clean: check
	rm -rf $(BUILD_DIR)/$(BOARD_DIR)
	
build/$(BOARD_DIR)/xvc_microserver.xpr: 
	mkdir -p $(BUILD_DIR)
	
	cd $(BUILD_DIR); $(XILINX_VIVADO)/bin/vivado -mode batch -source $(TCL)/create_hardware_$(BOARD_TYPE).tcl -tclargs  -jobs $(JOBS) -build_dir $(BOARD_DIR) -board_name $(BOARD_NAME) -board_ver $(BOARD_VER) -board_part $(BOARD_PART)
	
hardware: check check build/$(BOARD_DIR)/xvc_microserver.xpr
	

build/$(BOARD_DIR)/xvc_microserver.runs/impl_1/: build/$(BOARD_DIR)/xvc_microserver.xpr 
	cd $(BUILD_DIR); $(XILINX_VIVADO)/bin/vivado -mode batch -source $(TCL)/create_bitstream_$(BOARD_TYPE).tcl -tclargs -jobs $(JOBS) -build_dir $(BOARD_DIR) -board_name $(BOARD_NAME) -board_ver $(BOARD_VER) -board_part $(BOARD_PART)
bitstream: check build/$(BOARD_DIR)/xvc_microserver.runs/impl_1/

build/$(BOARD_DIR)/xvc_microserver.sdk/: build/$(BOARD_DIR)/xvc_microserver.runs/impl_1/

	# Copying bitstram and creating sdk structure
	mkdir $(BUILD_DIR)/$(BOARD_DIR)/xvc_microserver.sdk
	cp    $(BUILD_DIR)/$(BOARD_DIR)/xvc_microserver.runs/impl_1/*.sysdef $(BUILD_DIR)/$(BOARD_DIR)/xvc_microserver.sdk/base_wrapper.hdf

	cd $(BUILD_DIR); $(XILINX_SDK)/bin/xsct $(TCL)/create_sdk.tcl -jobs $(JOBS) -build_dir $(BOARD_DIR) -board_name $(BOARD_NAME) -board_ver $(BOARD_VER) -board_part $(BOARD_PART)

	cp -R src/*                                 $(BUILD_DIR)/$(BOARD_DIR)/xvc_microserver.sdk/xvc_microserver_app/src/
	#cp -R xvcd/src                             $(BUILD_DIR)/$(BOARD_DIR)/xvc_microserver.sdk/xvc_microserver_app/xvcd/
	cp -R uip/uip                               $(BUILD_DIR)/$(BOARD_DIR)/xvc_microserver.sdk/xvc_microserver_app/uip/
	rm -f $(BUILD_DIR)/$(BOARD_DIR)/xvc_microserver.sdk/xvc_microserver_app/uip/uip-split.*
	cp -R uip/apps/hello-world/hello-world.c    $(BUILD_DIR)/$(BOARD_DIR)/xvc_microserver.sdk/xvc_microserver_app/src/
	cp -R uip/apps/hello-world/hello-world.h    $(BUILD_DIR)/$(BOARD_DIR)/xvc_microserver.sdk/xvc_microserver_app/src/
	
	cd $(BUILD_DIR); $(XILINX_SDK)/bin/xsct $(TCL)/create_app.tcl -jobs $(JOBS) -build_dir $(BOARD_DIR) -board_name $(BOARD_NAME) -board_ver $(BOARD_VER) -board_part $(BOARD_PART)
	
sdk: check build/$(BOARD_DIR)/xvc_microserver.sdk/ 


clean-sdk: check
	rm -rf $(BUILD_DIR)/$(BOARD_DIR)/xvc_microserver.sdk/

copyback-sdk: check
	cp $(BUILD_DIR)/$(BOARD_DIR)/xvc_microserver.sdk/xvc_microserver_app/src/*  src/
	rm src/lscript.ld
	rm src/README.txt
