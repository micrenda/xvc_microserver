
#ifndef __TAPDEV_H__
#define __TAPDEV_H__

#include <xaxiethernet.h>
#include <xaxidma.h>
#include <xparameters.h>


// TODO: To check this numbers
#define RXBD_CNT				128	/* Number of RxBDs to use */
#define TXBD_CNT				128	/* Number of TxBDs to use */
#define MAX_PKT_LEN		0x200


// Number of bytes to reserve for BD space for the number of BDs desired
#define RXBD_SPACE_BYTES (XAxiDma_BdRingMemCalc(XAXIDMA_BD_MINIMUM_ALIGNMENT, RXBD_CNT))
#define TXBD_SPACE_BYTES (XAxiDma_BdRingMemCalc(XAXIDMA_BD_MINIMUM_ALIGNMENT, TXBD_CNT))


//char curre_mac_address[6] = { 0x00, 0x0A, 0x35, 0x01, 0x02, 0x03 };

char rx_bd_space[RXBD_SPACE_BYTES] __attribute__ ((aligned(XAXIDMA_BD_MINIMUM_ALIGNMENT)));
char tx_bd_space[TXBD_SPACE_BYTES] __attribute__ ((aligned(XAXIDMA_BD_MINIMUM_ALIGNMENT)));


XAxiEthernet	ethernet;
XAxiDma 		ethernet_dma;

XAxiDma_BdRing  bd_ring_rx;
XAxiDma_BdRing  bd_ring_tx;

XAxiDma_Bd bd_template;

void tapdev_init(void);
unsigned int tapdev_read(void);
void tapdev_send(void);

#endif /* __TAPDEV_H__ */
