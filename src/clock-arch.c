
#include "clock-arch.h"
#include <xtmrctr.h>



/*---------------------------------------------------------------------------*/
clock_time_t clock_time(void)
{
  // Return the current time from board turn on in milliseconds
  return XTmrCtr_GetValue(&global_timer, 0) * 1000 / XPAR_CPU_M_AXI_DP_FREQ_HZ;
}
/*---------------------------------------------------------------------------*/
