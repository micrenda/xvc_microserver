
#ifndef __CLOCK_ARCH_H__
#define __CLOCK_ARCH_H__

#include <xtmrctr.h>

#define CLOCK_CONF_SECOND XPAR_CPU_CORE_CLOCK_FREQ_HZ

typedef int clock_time_t;
XTmrCtr global_timer;

#endif /* __CLOCK_ARCH_H__ */
