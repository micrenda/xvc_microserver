
#include "uip.h"
#include "tapdev.h"
#include <xaxiethernet.h>
#include <xparameters.h>
#include <mb_interface.h>
#include <xstatus.h>
#include <stdlib.h>

/*---------------------------------------------------------------------------*/
void tapdev_init_tx(void)
{
	XAxiDma_IntrDisable(&ethernet_dma, XAXIDMA_IRQ_ALL_MASK, XAXIDMA_DMA_TO_DEVICE);
	XAxiDma_BdRingIntDisable(&bd_ring_tx,  XAXIDMA_IRQ_ALL_MASK);

	int ring_create_status  = XAxiDma_BdRingCreate(&bd_ring_tx, (UINTPTR) &tx_bd_space, (UINTPTR) &tx_bd_space, XAXIDMA_BD_MINIMUM_ALIGNMENT, RXBD_CNT);
	if (ring_create_status != XST_SUCCESS)
	{
		xil_printf("Error eth init: error creating TxBD space\n");
		exit(1);
	}

	XAxiDma_BdClear(&bd_template);

	int ring_clone_status  = XAxiDma_BdRingClone(&bd_ring_tx, &bd_template);
	if (ring_clone_status != XST_SUCCESS)
	{
		xil_printf("Error eth init: error cloning TxBD space\n");
		exit(1);
	}

}
/*---------------------------------------------------------------------------*/
void tapdev_init_rx(void)
{
	XAxiDma_IntrDisable(&ethernet_dma,  XAXIDMA_IRQ_ALL_MASK, XAXIDMA_DEVICE_TO_DMA);
	XAxiDma_BdRingIntDisable(&bd_ring_rx,  XAXIDMA_IRQ_ALL_MASK);

	int ring_create_status  = XAxiDma_BdRingCreate(&bd_ring_rx, (UINTPTR) &rx_bd_space, (UINTPTR) &rx_bd_space, XAXIDMA_BD_MINIMUM_ALIGNMENT, RXBD_CNT);
	if (ring_create_status != XST_SUCCESS)
	{
		xil_printf("Error eth init: error creating RxBD space\n");
		exit(1);
	}

	XAxiDma_BdClear(&bd_template);

	int ring_clone_status  = XAxiDma_BdRingClone(&bd_ring_rx, &bd_template);
	if (ring_clone_status != XST_SUCCESS)
	{
		xil_printf("Error eth init: error cloning RxBD space\n");
		exit(1);
	}
}
/*---------------------------------------------------------------------------*/
void tapdev_init(void)
{
	// Initializing Ethernet
	XAxiEthernet_Config *ethernet_config = XAxiEthernet_LookupConfig(XPAR_AXI_ETHERNET_0_DEVICE_ID);
	if (!ethernet_config)
	{
		xil_printf("Error eth init: no config found\n");
		exit(1);
	}

	int ethernet_status = XAxiEthernet_CfgInitialize(&ethernet, ethernet_config, ethernet_config->BaseAddress);
	if (ethernet_status != XST_SUCCESS)
	{
		xil_printf("Error eth init: initialization failed\n");
		exit(1);
	}


	// Initializing DMA
	XAxiDma_Config *ethernet_dma_config = XAxiDma_LookupConfig(XPAR_AXIDMA_0_DEVICE_ID);
	if (!ethernet_dma_config)
	{
		xil_printf("Error during DMA initialization: no config found\n");
		exit(1);
	}

	int ethernet_dma_status = XAxiDma_CfgInitialize(&ethernet_dma, ethernet_dma_config);
	if (ethernet_dma_status != XST_SUCCESS)
	{
		xil_printf("Error during DMA initialization: initialization failed\n");
		exit(1);
	}

	if(!XAxiDma_HasSg(&ethernet_dma))
	{
		xil_printf("Error during DMA initialization: device is not configured as SG mode\n");
		exit(1);
	}

	//int ethernet_mac_status = XAxiEthernet_SetMacAddress(&ethernet, curre_mac_address);
	//if (ethernet_mac_status != XST_SUCCESS)
	//{
	//	xil_printf("Error eth init: error setting MAC address");
	//	exit(1);
	//}

	bd_ring_rx = *XAxiDma_GetRxRing(&ethernet_dma);
	bd_ring_tx = *XAxiDma_GetTxRing(&ethernet_dma);

	tapdev_init_tx();
	tapdev_init_rx();


	XAxiEthernet_Start(&ethernet);

	int bd_ring_rx_start_status = XAxiDma_BdRingStart(&bd_ring_rx);
	if (bd_ring_rx_start_status != XST_SUCCESS)
	{
		xil_printf("Error eth init: error starting rx ring");
		exit(1);
	}

	int bd_ring_tx_start_status = XAxiDma_BdRingStart(&bd_ring_tx);
	if (bd_ring_tx_start_status != XST_SUCCESS)
	{
		xil_printf("Error eth init: error starting tx ring");
		exit(1);
	}

}
/*---------------------------------------------------------------------------*/
unsigned int tapdev_read(void)
{
	int status = XAxiDma_SimpleTransfer(&ethernet_dma, (UINTPTR)uip_buf, UIP_BUFSIZE, XAXIDMA_DEVICE_TO_DMA);

	if (status != XST_SUCCESS)
		return 0;
	else
		return UIP_BUFSIZE;

}
/*---------------------------------------------------------------------------*/
void tapdev_send(void)
{
	int len = uip_len < UIP_BUFSIZE ? uip_len : UIP_BUFSIZE;
	if (len > 0)
		XAxiDma_SimpleTransfer(&ethernet_dma, (UINTPTR)&uip_buf, len, XAXIDMA_DMA_TO_DEVICE);
}
/*---------------------------------------------------------------------------*/
